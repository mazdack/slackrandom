package com.mazdack.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;

public class RollDto {
  @JsonProperty
  String uuid;
  @JsonProperty
  List<String> input;
  @JsonProperty
  List<List<String>> rolls;
  @JsonProperty
  LocalDateTime creationDate;

  public RollDto(String uuid, List<String> input, List<List<String>> rolls, LocalDateTime creationDate) {
    this.uuid = uuid;
    this.input = input;
    this.rolls = rolls;
    this.creationDate = creationDate;
  }
}
