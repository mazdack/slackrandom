package com.mazdack.resource;

import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.api.model.block.element.ButtonElement;
import com.github.seratch.jslack.app_backend.slash_commands.payload.SlashCommandPayload;
import com.github.seratch.jslack.app_backend.slash_commands.payload.SlashCommandPayloadParser;
import com.github.seratch.jslack.app_backend.slash_commands.response.SlashCommandResponse;
import com.mazdack.entity.Roll;
import com.mazdack.service.RollService;
import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@Path("/random")
@Produces({MediaType.APPLICATION_JSON})
public class RandomCommandResource {
  @Autowired
  RollService rollService;
  @Autowired
  SlashCommandPayloadParser payloadParser;

  private static Logger commandLog = LoggerFactory.getLogger("commandLogger");

  @POST
  public SlashCommandResponse randomCommand(String requestBody) {
    SlashCommandPayload commandPayload = payloadParser.parse(requestBody);
    if (!StringUtils.hasText(commandPayload.getText())) {
      SectionBlock sectionBlock = SectionBlock.builder().text(PlainTextObject.builder().text("Please provide space-separated list to randomize").build()).build();
      return SlashCommandResponse.builder().blocks(List.of(sectionBlock)).responseType("ephemeral").build();
    }

    commandLog.info(commandPayload.toString());
    Roll roll = rollService.roll(commandPayload.getText());

    List<String> lastRoll = roll.getRolls().get(roll.getRolls().size() - 1);

    List<LayoutBlock> blocks = List.of(
      SectionBlock.builder()
        .text(MarkdownTextObject.builder().text(String.format("Rolled dices and randomized result is *%s*", lastRoll)).build())
        .accessory(
          ButtonElement.builder().text(PlainTextObject.builder().text("Details").build()).value(roll.getUuid()).build()
        )
        .build()
    );

    return SlashCommandResponse.builder()
      .blocks(blocks)
      .responseType("in_channel")
      .build();
  }

}
