package com.mazdack.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.seratch.jslack.api.model.block.LayoutBlock;
import com.github.seratch.jslack.api.model.block.SectionBlock;
import com.github.seratch.jslack.api.model.block.composition.MarkdownTextObject;
import com.github.seratch.jslack.api.model.block.composition.PlainTextObject;
import com.github.seratch.jslack.app_backend.interactive_messages.payload.AttachmentActionPayload;
import com.github.seratch.jslack.app_backend.interactive_messages.response.ActionResponse;
import com.mazdack.repositories.RollRepository;
import java.io.IOException;
import static java.lang.String.format;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Path("/interactive-components")
@Produces(MediaType.APPLICATION_JSON)
public class InteractiveComponentsResource {

  private static Logger LOG = LoggerFactory.getLogger(InteractiveComponentsResource.class);
  @Autowired
  private RestTemplate restTemplate;
  @Autowired
  private RollRepository rollRepository;
  @Autowired
  private ObjectMapper objectMapper;

  @POST
  public ResponseEntity<Object> doAll(@FormParam("payload") String payload) throws IOException {
    AttachmentActionPayload request = objectMapper.readValue(payload, AttachmentActionPayload.class);
    final HttpStatus[] httpStatus = {HttpStatus.OK};

    LOG.info("request {}", request.toString());
    LOG.info("payload {}",payload);

    String rollUuid = request.getActions().get(0).getValue();
    AtomicBoolean deleteOriginal = new AtomicBoolean(true);
    AtomicBoolean replaceOriginal = new AtomicBoolean(true);

    List<LayoutBlock> blocks = List.of(rollRepository.findById(rollUuid).map(roll -> {
      String rolls = roll.getRolls().stream().map(Objects::toString).collect(Collectors.joining(", "));
      String response = format("<@%s> here are details. Initial data was *%s*, and random shuffles were *%s*", request.getUser().getId(), roll.getInput().toString(), rolls);
      return SectionBlock.builder()
        .text(MarkdownTextObject.builder().text(response).build())
        .build();
    }).orElseGet(() -> {
      httpStatus[0] = HttpStatus.NOT_FOUND;
      deleteOriginal.set(false);
      replaceOriginal.set(false);
      return SectionBlock.builder()
        .text(PlainTextObject.builder().text(format("Can't find roll uuid = %s", rollUuid)).build())
        .build();
    }));

    ActionResponse actionResponse = ActionResponse.builder()
      .deleteOriginal(deleteOriginal.get())
      .replaceOriginal(replaceOriginal.get())
      .responseType("in_channel")
      .blocks(blocks)
      .build();
    LOG.info("Sending result {}, to response_url {}", new ObjectMapper().writeValueAsString(actionResponse), request.getResponseUrl());
    ResponseEntity<String> responseEntity = restTemplate.exchange(request.getResponseUrl(), HttpMethod.POST, new HttpEntity<>(actionResponse), String.class);

    LOG.info("response code {}, result {}", responseEntity.getStatusCode(), responseEntity.getBody());

    return new ResponseEntity<>(httpStatus[0]);
  }
}
