package com.mazdack.resource;

import com.mazdack.dto.RollDto;
import com.mazdack.repositories.RollRepository;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/rolls")
@Produces(MediaType.APPLICATION_JSON)
public class RollResource {
  @Autowired
  RollRepository rollRepository;
  @GET
  public List<RollDto> getRolls() {
    return StreamSupport.stream(rollRepository.findAll().spliterator(), false)
        .map(roll -> new RollDto(roll.getUuid(), roll.getInput(), roll.getRolls(), roll.getCreationTime()))
        .collect(Collectors.toList());
  }
}
