package com.mazdack.service;

import com.mazdack.entity.Roll;
import com.mazdack.repositories.RollRepository;
import static java.util.Arrays.stream;
import java.util.Collections;
import java.util.List;
import static java.util.function.Predicate.not;
import static java.util.stream.Collectors.toList;
import java.util.stream.IntStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RollService {
  @Value("${roll.times:3}")
  int timesToRoll;
  @Autowired
  RollRepository rollRepository;

  public Roll roll(String text) {
    List<String> input = stream(text.split(" "))
        .filter(not(String::isEmpty))
        .map(item -> item.replaceAll(",$", ""))
        .collect(toList());
    Roll roll = new Roll();
    roll.setInput(input);
    IntStream.range(0, timesToRoll).forEach((i) -> {
        Collections.shuffle(input);
        roll.addRoll(input);
    });

    rollRepository.save(roll);

    return roll;
  }
}
