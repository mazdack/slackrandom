package com.mazdack.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.data.annotation.Id;
import org.springframework.data.keyvalue.annotation.KeySpace;

@KeySpace("roll")
public class Roll {
  @Id
  String uuid = UUID.randomUUID().toString();
  List<String> input;
  List<List<String>> rolls = new ArrayList<>();
  LocalDateTime creationTime = LocalDateTime.now();

  public String getUuid() {
    return uuid;
  }

  public List<String> getInput() {
    return input;
  }

  public void setInput(List<String> input) {
    this.input = new ArrayList<>(input);
  }

  public List<List<String>> getRolls() {
    return rolls;
  }

  public void addRoll(List<String> roll) {
    this.rolls.add(new ArrayList<>(roll));
  }

  public LocalDateTime getCreationTime() {
    return creationTime;
  }

  public void setCreationTime(LocalDateTime creationTime) {
    this.creationTime = creationTime;
  }
}
