package com.mazdack.repositories;

import com.mazdack.entity.Roll;
import org.springframework.data.repository.CrudRepository;

public interface RollRepository extends CrudRepository<Roll, String> {
}
