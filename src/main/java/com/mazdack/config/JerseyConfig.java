package com.mazdack.config;

import com.mazdack.resource.InteractiveComponentsResource;
import com.mazdack.resource.RandomCommandResource;
import com.mazdack.resource.RollResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {
  public JerseyConfig() {
    register(RandomCommandResource.class);
    register(RollResource.class);
    register(InteractiveComponentsResource.class);
  }
}
