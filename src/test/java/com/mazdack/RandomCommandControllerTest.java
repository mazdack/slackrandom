package com.mazdack;

import com.jayway.jsonpath.JsonPath;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RandomCommandControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

  private Pattern pattern = Pattern.compile(".*\\[(.*)\\]");

	@Test
	public void randomFiltersEmptyStrings() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("text", "first second       third");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		String response = this.restTemplate.postForObject("/random", request, String.class);

    assertEquals("section", JsonPath.read(response, "$.blocks[0].type"));
    String text = JsonPath.<String>read(response, "$.blocks[0].text.text");

    Matcher matcher = pattern.matcher(text);
    assertTrue(matcher.find());
    assertEquals(3, matcher.group(1).split(" ").length);
	}

	@Test
	public void emptyTextReturnsError() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("text", "       ");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
    assertTrue(this.restTemplate.postForObject("/random", request, String.class).contains("Please provide space-separated list to randomize"));
	}
}
